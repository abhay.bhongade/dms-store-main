import {
  persistReducer,
  PURGE,
  PAUSE,
  PERSIST,
  FLUSH,
  REGISTER,
  REHYDRATE,
} from "redux-persist";
import { combineReducers, configureStore } from "@reduxjs/toolkit";
import storage from "redux-persist/lib/storage";

import ReportDataReducer from "../features/ReportReducers/ReportDataSlice";
import CustomerScoredCardReducer from "../features/CustomerScoredCardReducers/CustomerScoredCardSlice";
import ReportScoreReducer from "../features/ReportReducers/ReportScoreSlice";

let persistConfig = {
  key: "root",
  version: 1,
  storage,
};

const reducer = combineReducers({
  ReportData: ReportDataReducer,
  CustomerScoredCard: CustomerScoredCardReducer,
  ReportScore: ReportScoreReducer,
});

let persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoreActions: [PURGE, PAUSE, PERSIST, FLUSH, REGISTER, REHYDRATE],
      },
    }),
});
