import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { API, config } from "../../../Services/index";
import { toast } from "react-toastify";

const initialState = {
  data: {},
  loading: false,
  error: "",
};

export const ReportData = createAsyncThunk(
  "api/ReportData",
  async (payloadObj) => {
    try {
      const response = await axios.post(
        API + "getDbInventoryStockStatusValue",
        payloadObj,
        config
      );
      console.log("response", response);
      return response.data;
    } catch (error) {
      console.log("error", error);
      const customId = "Custom-id-yes";
      toast.error(error?.response?.data?.message, {
        toastId: customId,
      });
      console.error("An error occurred3:", error?.response?.data?.message);
    }
  }
);

const ReportDataSlice = createSlice({
  name: "ReportData",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(ReportData.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(ReportData.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
      state.error = "";
    });
    builder.addCase(ReportData.rejected, (state, action) => {
      state.loading = false;
      state.data = [];
      state.error = action.error.message;
    });
  },
});

export default ReportDataSlice.reducer;
