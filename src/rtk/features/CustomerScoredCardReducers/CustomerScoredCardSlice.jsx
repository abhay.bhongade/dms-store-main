import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { API, config } from "../../../Services/index";
import { toast } from "react-toastify";

const initialState = {
  data: {},
  loading: false,
  error: "",
};

export const CustomerScoredCard = createAsyncThunk(
  "api/CustomerScoredCard",
  async (payloadObj) => {
    try {
      const response = await axios.post(
        API + "getCustomerbusinessSummaryData",
        payloadObj,
        config
      );
      console.log("response", response);
      return response.data;
    } catch (error) {
      const customId = "Custom-id-yes";

      console.log("error", error);
      console.error("An error occurred3:", error?.response?.data?.message);
      toast.error(error?.response?.data?.message, {
        toastId: customId,
      });
    }
  }
);

const CustomerScoredCardSlice = createSlice({
  name: "CustomerScoredCard",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(CustomerScoredCard.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase(CustomerScoredCard.fulfilled, (state, action) => {
      state.loading = false;
      state.data = action.payload;
      state.error = "";
    });
    builder.addCase(CustomerScoredCard.rejected, (state, action) => {
      state.loading = false;
      state.data = [];
      state.error = action.error.message;
    });
  },
});

export default CustomerScoredCardSlice.reducer;
