import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import "./App.css";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { BrowserRouter } from "react-router-dom";
import { store } from "../src/rtk/app/store.js";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";
import { ScrollToTop } from "../src/Constants/ScrollToTop.jsx";

let persistor = persistStore(store);
const isMobile = window.innerWidth <= 540;
const toastPosition = isMobile ? "bottom-center" : "top-center";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <BrowserRouter>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <ScrollToTop />
          <App />
          <ToastContainer
            position={toastPosition}
            autoClose={900}
            limit={1}
            hideProgressBar
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss={false}
            draggable
            pauseOnHover
            theme="colored"
          />
        </PersistGate>
      </Provider>
    </BrowserRouter>
  </React.StrictMode>
);

//http://localhost:3000/report?token=11103|eGTer7FfEYWtB7tNgwkRTBhu3XRBvaTYYDRZ4fwS&id=49
