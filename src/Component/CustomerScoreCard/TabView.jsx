import React, { useState, useEffect } from "react";
import { Tab, Tabs } from "react-bootstrap";
import StatisticCard from "./StatisticCard";
import ReusableTable from "./ReusableTable";
import "../../Component/Styles/CustomerScoreCard.css";
import {
  Public as PublicIcon,
  Search as SearchIcon,
  ShoppingCart as ShoppingCartIcon,
  LocalShipping as LocalShippingIcon,
  ViewInAr as ViewInArIcon,
  Sell as SellIcon,
  Add as AddIcon,
  Remove as RemoveIcon,
  Call as CallIcon,
} from "@mui/icons-material";
import { useNavigate } from "react-router-dom";
import Loading from "../Loading/Loading";
import { useDispatch, useSelector } from "react-redux";
import { CustomerScoredCard } from "../../rtk/features/CustomerScoredCardReducers/CustomerScoredCardSlice";
import { toast } from "react-toastify";
import { getDistributorId } from "../../Services/index";

const TabView = () => {
  const iconStyle = { color: "white" };
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const customId = "Custom-id-yes";
  const [loading, setLoading] = useState(false);
  const [fetchedData, setFetchedData] = useState([]);
  const [key, setKey] = useState("");
  const reportData = useSelector((state) => state.ReportScore?.data?.data);
  const distributorId = getDistributorId();

  useEffect(() => {
    const tabName = localStorage.getItem("tabName");
    setKey(tabName);
  }, []);

  const payloadObj = {
    distributor_id: distributorId,
    from_date: reportData?.from_date,
    to_date: reportData?.to_date,
  };

  const handleNavigate = async (path, status_value, tag_status) => {
    const payload = {
      ...payloadObj,
      tag_status: tag_status,
      status_value: status_value,
    };
    try {
      const actionResult = await dispatch(CustomerScoredCard(payload));
      if (actionResult?.payload?.success) {
        navigate(path);
      } else if (actionResult?.payload?.success === false) {
        toast.error(actionResult?.payload?.message, { toastId: customId });
      }
    } catch (error) {
      console.log("error while fetching data", error);
    }
  };
  console.log("key", key);

  const payload = {
    ...payloadObj,
    tag_status: key,
    status_value: 1,
  };

  useEffect(() => {
    if (key) {
      localStorage.setItem("tabName", key);
      setLoading(true);
      dispatch(CustomerScoredCard(payload))
        .then(() => {
          setLoading(false);
        })
        .catch((error) => {
          console.log("error while fetching data in tab", error);
        });
    }
  }, [key]);

  const custScoredData = useSelector(
    (state) => state.CustomerScoredCard?.data?.data
  );

  console.log("custScoredData", custScoredData);

  return (
    <div>
      {loading ? (
        <Loading heightofScreen="50vh" />
      ) : fetchedData ? (
        <Tabs
          activeKey={key}
          onSelect={(k) => setKey(k)}
          className="mb-3"
          fill="true"
        >
          <Tab eventKey="delivery" title="Delivery" fill="true">
            {key === "delivery" && (
              <>
                <div className="row row-flex">
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Total District"
                      value={custScoredData?.district_count || 0}
                      icon={
                        <PublicIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      onClick={() =>
                        handleNavigate(
                          "/next_page/total-district-table",
                          "2",
                          "delivery"
                        )
                      }
                      className="statistic-card"
                      additionalClass="d-total-district-bg"
                      header={true}
                    />
                  </div>
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Total Beat"
                      value={custScoredData?.beat_count || 0}
                      icon={
                        <SearchIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      onClick={() =>
                        handleNavigate(
                          "/next_page/total-beats-table",
                          "3",
                          "delivery"
                        )
                      }
                      className="statistic-card"
                      additionalClass="d-total-order-amt-bg"
                      header={true}
                    />
                  </div>
                </div>
                <div className="row row-flex">
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Total Order Amount"
                      value={`₹${custScoredData?.order_amount || 0}`}
                      icon={
                        <ShoppingCartIcon
                          sx={{ fontSize: 16 }}
                          style={iconStyle}
                        />
                      }
                      className="statistic-card"
                      additionalClass="d-total-order-amt-bg"
                      header={true}
                    />
                  </div>
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Total Delivery Amount"
                      value={`₹${custScoredData?.delivery_amount || 0}`}
                      icon={
                        <LocalShippingIcon
                          sx={{ fontSize: 16 }}
                          style={iconStyle}
                        />
                      }
                      className="statistic-card"
                      additionalClass="d-total-delivery-amt-bg"
                      header={true}
                    />
                  </div>
                </div>
                <div className="row row-flex">
                  <div className="col-12 col-flex">
                    <StatisticCard
                      title="Total Delivery Percentage"
                      value={`${custScoredData?.delivery_percentage || 0}%`}
                      icon={
                        <ViewInArIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      className="statistic-card"
                      additionalClass="d-total-delivery-percntg-bg "
                      header={true}
                    />
                  </div>
                </div>
              </>
            )}
          </Tab>
          <Tab eventKey="escalation" title="Escalation" fill="true">
            {key === "escalation" && (
              <>
                <div className="row row-flex">
                  <div className="col col-flex">
                    <StatisticCard
                      title="Total Escalation Count"
                      value={custScoredData?.total_escalation_count || 0}
                      icon={
                        <SellIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      onClick={() =>
                        handleNavigate(
                          "/next_page/escalation-count-table",
                          "2",
                          "escalation"
                        )
                      }
                      className="statistic-card"
                      additionalClass="e-total-escal-count-bg"
                      header={true}
                    />
                  </div>
                </div>
                <div className="row row-flex">
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Positive Escalation Count"
                      value={custScoredData?.positive_escalation_count || 0}
                      icon={<AddIcon sx={{ fontSize: 16 }} style={iconStyle} />}
                      onClick={() =>
                        handleNavigate(
                          "/next_page/positive-escalation-table",
                          "3",
                          "escalation"
                        )
                      }
                      className="statistic-card"
                      additionalClass="e-postv-escal-count-bg"
                      header={true}
                    />
                  </div>
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Negative Escalation Count"
                      value={custScoredData?.negative_escalation_count || 0}
                      icon={
                        <RemoveIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      onClick={() =>
                        handleNavigate(
                          "/next_page/negative-escalation-table",
                          "4",
                          "escalation"
                        )
                      }
                      className="statistic-card"
                      additionalClass="e-negtv-escal-count-bg"
                      header={true}
                    />
                  </div>
                </div>
              </>
            )}
          </Tab>
          {/* <Tab eventKey="callingDay" title="Calling Day" fill="true">
            {key === "callingDay" && (
              <>
                <div className="row row-flex">
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Total Calling Day"
                      value="7"
                      icon={
                        <CallIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      className="statistic-card"
                      additionalClass="total-calling-bg"
                      header={true}
                    />
                  </div>
                  <div className="col-6 col-flex">
                    <StatisticCard
                      title="Active Calling Day"
                      value="20"
                      icon={
                        <CallIcon sx={{ fontSize: 16 }} style={iconStyle} />
                      }
                      className="statistic-card"
                      additionalClass="active-calling-bg"
                      header={true}
                    />
                  </div>
                </div>
              </>
            )}
          </Tab> */}
        </Tabs>
      ) : (
        <div>No Data Found</div>
      )}
    </div>
  );
};

export default TabView;
