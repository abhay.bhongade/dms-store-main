import React, { useState, useEffect } from "react";
import {
  Modal,
  Box,
  Button,
  MenuItem,
  Select,
  FormControl,
  Dialog,
  DialogTitle,
  DialogContent,
  Grid,
  Typography,
  Grow,
} from "@mui/material";
import moment from "moment";
import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";

const style = {
  position: "fixed",
  top: { xs: "auto", sm: "50%" },
  bottom: { xs: 0, sm: "auto" },
  left: "50%",
  transform: { xs: "translate(-50%, 0)", sm: "translate(-50%, -50%)" },
  width: "100%",
  maxWidth: { xs: "100%", sm: 400 },
  bgcolor: "background.paper",
  boxShadow: 24,
  p: 4,
  display: "flex",
  flexDirection: "column",
  gap: 2,
  borderRadius: { xs: "16px 16px 0 0", sm: "8px" },
  padding: { xs: "14px 10px", sm: "14px 10px" },
};

const btnStyle = {
  bgcolor: "#F3505A",
  borderRadius: 14,
};

const DateFilterModal = ({ open, handleClose, handleFilter }) => {
  const currentYear = moment().year();
  const currentMonth = moment().month() + 1;

  const [year, setYear] = useState(currentYear);
  const [month, setMonth] = useState(currentMonth);
  const [yearDialogOpen, setYearDialogOpen] = useState(false);

  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  useEffect(() => {
    if (open) {
      document.body.classList.add("no-scroll");
    } else {
      document.body.classList.remove("no-scroll");
    }
    return () => {
      document.body.classList.remove("no-scroll");
    };
  }, [open]);

  const handleSubmit = () => {
    handleFilter({ year, month });
    handleClose();
  };

  const handleReset = () => {
    setYear(currentYear);
    setMonth(currentMonth);
    handleFilter({ year: currentYear, month: currentMonth });
    handleClose();
  };

  const handleYearClick = (selectedYear) => {
    setYear(selectedYear);
    if (selectedYear !== currentYear) {
      setMonth(1); // Reset month to January if a different year is selected
    } else {
      setMonth(currentMonth); // Reset month to current month if current year is selected
    }
    setYearDialogOpen(false);
  };

  const months = moment.months().filter((_, index) => {
    return year !== currentYear || index < currentMonth;
  });

  return (
    <>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <div className="content-space-between">
            <div className="filter_reset_button">Filter</div>
            <div className="filter_reset_button hover" onClick={handleReset}>
              Reset
            </div>
          </div>
          <FormControl fullWidth>
            <h6>Year</h6>
            <Select
              value={year}
              onClick={() => setYearDialogOpen(true)}
              readOnly
              sx={{
                borderRadius: "26px",
                backgroundColor: "#f7f7f7",
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
                "& .MuiOutlinedInput-root": {
                  "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
                    border: "none",
                  },
                },
              }}
              MenuProps={{
                PaperProps: {
                  style: {
                    borderRadius: "26px",
                  },
                },
              }}
            >
              <MenuItem value={year} style={{ backgroundColor: "#F3505A" }}>
                {year}
              </MenuItem>
            </Select>
          </FormControl>
          <FormControl fullWidth>
            <h6>Month</h6>
            <Select
              value={month}
              onChange={(e) => setMonth(e.target.value)}
              sx={{
                borderRadius: "26px",
                backgroundColor: "#f7f7f7",
                "& .MuiOutlinedInput-notchedOutline": {
                  border: "none",
                },
                "& .MuiOutlinedInput-root": {
                  "&.Mui-focused .MuiOutlinedInput-notchedOutline": {
                    border: "none",
                  },
                },
              }}
              MenuProps={{
                PaperProps: {
                  TransitionComponent: Grow,
                  className: "extraClass",
                  style: isMobile
                    ? {
                        minWidth: "100%",
                        borderRadius: "unset",
                        left: 0,
                        bottom: 0,
                        borderTopLeftRadius: "26px",
                        borderTopRightRadius: "26px",
                      }
                    : {
                        borderRadius: "26px",
                      },
                },
              }}
            >
              <MenuItem
                className="month_model"
                readOnly
                style={{
                  color: "#F3505A",
                  fontWeight: "500",
                }}
              >
                Select Month
              </MenuItem>
              {months.map((monthName, index) => (
                <MenuItem
                  className="month_model"
                  key={index}
                  value={index + 1}
                  style={{
                    backgroundColor:
                      month === index + 1 ? "#F3505A" : "inherit",
                    color: month === index + 1 ? "#fff" : "inherit",
                  }}
                >
                  {monthName}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <div className="content-in-center">
            <Button
              variant="contained"
              color="primary"
              onClick={handleSubmit}
              sx={btnStyle}
              style={{ padding: "6px 26px" }}
            >
              Done
            </Button>
          </div>
        </Box>
      </Modal>
      <Dialog open={yearDialogOpen} onClose={() => setYearDialogOpen(false)}>
        <DialogTitle>Select Year</DialogTitle>
        <DialogContent>
          <Grid container spacing={2}>
            {Array.from(
              { length: currentYear - 2014 },
              (_, index) => currentYear - index
            ).map((yr) => (
              <Grid item xs={4} key={yr}>
                <Button
                  fullWidth
                  variant={yr === year ? "contained" : "outlined"}
                  style={{
                    backgroundColor: yr === year ? "#F3505A" : "",
                  }}
                  onClick={() => handleYearClick(yr)}
                  sx={{ borderRadius: "26px" }}
                >
                  <Typography>{yr}</Typography>
                </Button>
              </Grid>
            ))}
          </Grid>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default DateFilterModal;

