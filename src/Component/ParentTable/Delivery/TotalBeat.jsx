import React from "react";
import CommonTableScoreCard from "../../UIElements/Table/CommonTableScoreCard";
import { useSelector } from "react-redux";

const TotalBeat = () => {
  const custScoredData = useSelector(
    (state) => state.CustomerScoredCard?.data?.data
  );

  const tabledata = custScoredData?.map((item, index) => ({
    sno: index + 1,
    beatName: item?.beat_name || "NA",
    orderAmount: `₹${item?.total_amount || 0} `,
    deliveryAmount: `₹${item?.delivery_amount || 0} `,
    deliveryPercentage: `${item?.delivery_percentage || 0}%`,
  }));

  const columns = [
    { label: "S.No", field: "sno" },
    { label: "Beat Name", field: "beatName" },
    { label: "Order Amount", field: "orderAmount" },
    { label: "Delivery Amount", field: "deliveryAmount" },
    { label: "Delivery Percentage", field: "deliveryPercentage" },
  ];

  return (
    <div className="container-fluid p-0 m-0">
      <div className="row">
        <div className="col-md-12">
          <CommonTableScoreCard
            columns={columns}
            tabledata={tabledata}
            tableheading="Beat"
          />
        </div>
      </div>
    </div>
  );
};

export default TotalBeat;
