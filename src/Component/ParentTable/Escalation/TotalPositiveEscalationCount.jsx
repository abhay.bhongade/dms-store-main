import React from "react";
import CommonTableScoreCard from "../../UIElements/Table/CommonTableScoreCard";
import { useSelector } from "react-redux";

const TotalPositiveEscalationCount = () => {
  const custScoredData = useSelector(
    (state) => state.CustomerScoredCard?.data?.data
  );

  const tabledata = custScoredData?.map((item, index) => ({
    sno: index + 1,
    orderBookingDate: item?.tag_date || "NA",
    retailerCode: item?.unique_code || "NA",
    retailerName: item?.retailer_name || "NA",
    escalationTag: item?.escalationtag_id || "NA",
    status: item?.status || "NA",
  }));

  const columns = [
    { label: "S.No", field: "sno" },
    { label: "Date", field: "orderBookingDate" },
    { label: "Retailer Code", field: "retailerCode" },
    { label: "Retailer Name", field: "retailerName" },
    { label: "Escalation Tag", field: "escalationTag" },
    { label: "Status", field: "status" },
  ];

  return (
    <div className="container-fluid p-0 m-0">
      <div className="row">
        <div className="col-md-12">
          <CommonTableScoreCard
            columns={columns}
            tabledata={tabledata}
            tableheading="Positive Escalation Count"
          />
        </div>
      </div>
    </div>
  );
};

export default TotalPositiveEscalationCount;
