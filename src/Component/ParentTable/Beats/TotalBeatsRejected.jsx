import React from "react";
import CommonTableScoreCard from "../../UIElements/Table/CommonTableScoreCard";
import { useSelector } from "react-redux";

const TotalBeatsRejected = () => {
  const data = useSelector((state) => state.ReportData?.data?.data);
  console.log("data", data);

  const tabledata = data?.map((item, index) => ({
    sno: index + 1,
    beatsName: item?.beat_name || "NA",
    orderBookingDate: item?.order_booking_date || "NA",
    orderAmount: `₹${item?.order_amount || 0} `,
    deliveryAmount: item?.delivery_amount || 0,
  }));

  const columns = [
    { label: "S.No", field: "sno" },
    { label: "Beats Name", field: "beatsName" },
    { label: "Order Booking Date", field: "orderBookingDate" },
    { label: "Order Amount", field: "orderAmount" },
    { label: "Delivery Amount", field: "deliveryAmount" },
  ];

  return (
    <div className="container-fluid p-0 m-0">
      <div className="row">
        <div className="col-md-12">
          <CommonTableScoreCard
            columns={columns}
            tabledata={tabledata}
            tableheading="Rejected Beats"
          />
        </div>
      </div>
    </div>
  );
};

export default TotalBeatsRejected;
