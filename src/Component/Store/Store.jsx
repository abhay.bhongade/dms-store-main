import React from "react";
import { useNavigate } from "react-router-dom";

const Store = () => {
  const navigate = useNavigate();
  const handleNavigate = (path) => {
    navigate(`${path}`);
  };
  return (
    <div className="container my-5">
      <div className="row">
        <div className="col-4"></div>
        <div className="col-4">
          <div className="px-2 store-link">
            <h4 onClick={() => handleNavigate("/report")}>View</h4>
          </div>
        </div>
        <div className="col-4"></div>
        {/* <div className="d-flex justify-center items-center m-5">
            <div className="px-2 store-link">
              <h4 onClick={() => handleNavigate("/store-view")}>View</h4>
            </div>
            <div className="px-2 store-link">
              <h4 onClick={() => handleNavigate("/store-view-scroll")}>
                Update
              </h4>
            </div>
          </div> */}
      </div>
    </div>
  );
};

export default Store;
