import React from "react";
import { CircularProgress } from "@mui/material";

const Loading = ({ heightofScreen = "100vh" }) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-md-12">
          <div className="content-center" style={{ height: heightofScreen }}>
            <CircularProgress
              style={{ width: "35px", height: "35px", color: "#F3505A" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Loading;
