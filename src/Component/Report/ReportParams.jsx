// import React, { useState, useEffect } from "react";
// import axios from "axios";
// import SectionHeading from "./SectionHeading";
// import DataRow from "./DataRow";
// import Card from "./Card";
// import "../Styles/Report.css";
// import "../Styles/CustomerScoreCard.css";
// import Chart from "../Charts/Chart";
// import { API, config, getDistributorId } from "../../Services/index";
// import moment from "moment";
// import { DateFilterModal } from "../UIElements/index";
// import calenderIcon from "../../assets/Calendar.svg";
// import Loading from "../Loading/Loading";
// import { ReportScore } from "../../rtk/features/ReportReducers/ReportScoreSlice";
// import { useDispatch, useSelector } from "react-redux";
// import { useLocation, useNavigate } from "react-router-dom";

// const Report = () => {
//   const location = useLocation();
//   const navigate = useNavigate();
//   const searchParams = new URLSearchParams(location.search);
//   const token = searchParams.get("token");
//   const id = searchParams.get("id");

//   const [fetchedData, setFetchedData] = useState([]);
//   const [isModalOpen, setIsModalOpen] = useState(false);
//   const [currentDate, setCurrentDate] = useState(moment());
//   const [loading, setLoading] = useState(false);
//   const dispatch = useDispatch();
//   const reportData = useSelector((state) => state.ReportScore?.data?.data);

//   useEffect(() => {
//     if (token && id) {
//       localStorage.setItem("token", token);
//       localStorage.setItem("id", id);
//     }
//   }, [token, id]);

//   useEffect(() => {
//     setFetchedData(reportData || []);
//   }, [reportData]);

//   const handleOpen = () => setIsModalOpen(true);
//   const handleClose = () => setIsModalOpen(false);

//   const handleFilter = (filterData) => {
//     const { year, month } = filterData;
//     setCurrentDate(moment({ year, month: month - 1 }));
//     fetchData({ year, month });
//   };

//   const distributorId = getDistributorId();

//   const fetchData = async ({ year, month }) => {
//     const payload = {
//       distributor_id: distributorId,
//       year: year,
//       month: moment()
//         .month(month - 1)
//         .format("MMMM"),
//     };

//     try {
//       setLoading(true);
//       const result = await dispatch(ReportScore(payload));
//       console.log("result", result);
//     } catch (error) {
//       console.log("error while fetching data", error);
//     } finally {
//       setLoading(false);
//     }
//   };

//   // useEffect(() => {
//   //   if (token && id) {
//   //     fetchData({ year: currentDate.year(), month: currentDate.month() + 1 });
//   //   }
//   // }, [dispatch, currentDate, token, id]);

//   useEffect(() => {
//     fetchData({ year: currentDate.year(), month: currentDate.month() + 1 });
//   }, []);

//   const data = [
//     {
//       id: 1,
//       mainHeading: "Escalations",
//       bodyContent: "Escalations Counting",
//       DynamicValue: fetchedData?.escalation_count || 0,
//       path: true,
//     },
//     {
//       id: 2,
//       mainHeading: "Inventory",
//       bodyContent: "Available OB Stock Value",
//       DynamicValue: `₹${fetchedData?.ob_stock_value || 0}`,
//       path: false,
//     },
//     {
//       id: 3,
//       mainHeading: "Primary billing",
//       bodyContent: "Pvt Label Contribution",
//       DynamicValue: fetchedData?.pvt_lable_contribution || "0%",
//       path: false,
//     },
//     {
//       id: 4,
//       mainHeading: "Counter Sale",
//       bodyContent: "Sale Amount",
//       DynamicValue: `₹${fetchedData?.counter_sale_value || 0} `,
//       path: false,
//     },
//   ];

//   console.log("fetchedData", fetchedData);

//   const payload = {
//     distributor_id: distributorId,
//     year: currentDate.year(),
//     month: currentDate.add(0, "months").format("MMMM"),
//   };

//   const handleNavigate = () => {
//     localStorage.setItem("tabName", "escalation");
//     navigate("/customer-score-card");
//   };

//   return (
//     <div className="container mt-2 mb-4">
//       {loading ? (
//         <Loading />
//       ) : fetchedData ? (
//         <>
//           <div className="row card mx-1">
//             <div className="col-12">
//               <div className="mb-2">
//                 <div className="your-score-heading text-center mt-2">
//                   Your Score
//                 </div>
//                 <div className="your-score text-center">
//                   {fetchedData?.score?.toFixed(2) || 0}
//                 </div>
//               </div>
//               <div className="content-in-center">
//                 <Chart score={fetchedData?.score?.toFixed(2) || 0} />
//               </div>
//             </div>
//           </div>

//           <div style={{ marginTop: "0.85rem" }}>
//             <div
//               className="card card-shadow calender-container"
//               style={{ borderRadius: "1rem", display: "inline-block" }}
//             >
//               <div className="card-body" style={{ padding: "0 0.7rem" }}>
//                 <div
//                   className="d-flex justify-content-center align-items-center p-1 "
//                   onClick={handleOpen}
//                   style={{ cursor: "pointer", flexDirection: "row" }}
//                 >
//                   <div className="calender-content">
//                     {currentDate.format("MMMM")}
//                   </div>
//                   <div className="calender-content mx-1">
//                     {currentDate.format("YYYY")}
//                   </div>
//                   <div>
//                     <img
//                       src={calenderIcon}
//                       alt="calendar icon"
//                       className="calendar-icon"
//                     />
//                   </div>
//                 </div>
//               </div>
//             </div>

//             <DateFilterModal
//               open={isModalOpen}
//               handleClose={handleClose}
//               handleFilter={handleFilter}
//             />
//           </div>

//           <div className="row">
//             <div className="col-md-12">
//               <Card title="Delivery">
//                 <SectionHeading heading="Beats" />
//                 <div className="row d-flex">
//                   <DataRow
//                     label="Ordered"
//                     value={fetchedData?.beats?.ordered_beat || 0}
//                     additionalClass="ms-2 me-1 hover"
//                     to="/beats-orders-table"
//                     type="beats"
//                     type_value="1"
//                     payload={payload}
//                   />
//                   <DataRow
//                     label="Delivered"
//                     value={fetchedData?.beats?.delivered_beat || 0}
//                     additionalClass="ms-1 me-2 hover"
//                     to="/beats-delivered-table"
//                     type="beats"
//                     type_value="2"
//                     payload={payload}
//                   />
//                 </div>
//                 <div className="row d-flex my-2">
//                   <DataRow
//                     label="Rejected"
//                     value={fetchedData?.beats?.rejected_beat || 0}
//                     additionalClass="ms-2 me-1 hover"
//                     to="/beats-rejected-table"
//                     type="beats"
//                     type_value="3"
//                     payload={payload}
//                   />
//                   <DataRow
//                     label="Pending"
//                     value={fetchedData?.beats?.pending_beat || 0}
//                     additionalClass="pending-bg ms-1 me-2 hover"
//                     dynamicBg={fetchedData?.beats?.beat_value_color}
//                     to="/beats-pending-table"
//                     type="beats"
//                     type_value="4"
//                     payload={payload}
//                   />
//                 </div>

//                 <SectionHeading heading="Orders" />
//                 <div className="row d-flex">
//                   <DataRow
//                     label="Total"
//                     value={fetchedData?.orders?.total_order || 0}
//                     additionalClass="ms-2 me-1 hover"
//                     to="/orders-total-table"
//                     type="orders"
//                     type_value="1"
//                     payload={payload}
//                   />
//                   <DataRow
//                     label="Accepted"
//                     value={fetchedData?.orders?.accepted_order || 0}
//                     additionalClass="ms-1 me-2 hover"
//                     to="/orders-accepted-table"
//                     type="orders"
//                     type_value="2"
//                     payload={payload}
//                   />
//                 </div>
//                 <div className="row my-2">
//                   <DataRow
//                     label="Rejected"
//                     value={fetchedData?.orders?.rejected_order || 0}
//                     additionalClass="ms-2 me-1 hover"
//                     to="/orders-rejected-table"
//                     type="orders"
//                     type_value="3"
//                     payload={payload}
//                   />
//                   <DataRow
//                     label="Pending"
//                     value={fetchedData?.orders?.pending_order || 0}
//                     additionalClass="pending-bg ms-1 me-2 hover"
//                     dynamicBg={fetchedData?.orders?.order_value_color}
//                     to="/orders-pending-table"
//                     type="orders"
//                     type_value="4"
//                     payload={payload}
//                   />
//                 </div>

//                 <SectionHeading heading="Amount" />
//                 <div className="row">
//                   <DataRow
//                     label={`Ordered - ₹${
//                       fetchedData?.amounts?.ordered_amount || 0
//                     }`}
//                     value=""
//                     additionalClass="mb-2 mx-2 py-3"
//                   />
//                 </div>
//                 <div className="row my-2">
//                   <DataRow
//                     label="Delivered"
//                     value={fetchedData?.amounts?.order_delivered || 0}
//                     additionalClass="ms-2 me-1"
//                   />
//                   <DataRow
//                     label="Delivery %"
//                     value={fetchedData?.amounts?.delivered_per || 0}
//                     additionalClass="delivery-bg ms-1 me-2"
//                     dynamicBg={fetchedData?.amounts?.delevery_per_color}
//                   />
//                 </div>
//               </Card>
//             </div>

//             <div className="col-md-12">
//               {data?.map((item) => (
//                 <Card title={item?.mainHeading} key={item?.id}>
//                   <div
//                     className="content-center card-content"
//                     onClick={() => item?.path && handleNavigate()}
//                   >
//                     {item?.bodyContent} - {item?.DynamicValue}
//                   </div>
//                 </Card>
//               ))}
//             </div>
//           </div>
//         </>
//       ) : (
//         <div>No Data Found</div>
//       )}
//     </div>
//   );
// };

// export default Report;
