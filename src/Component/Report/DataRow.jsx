import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { getReportData } from "../../Services/ReportData/ReportData";
import { API, config } from "../../Services/index";
import axios from "axios";
import { useDispatch } from "react-redux";
import { ReportData } from "../../rtk/features/ReportReducers/ReportDataSlice";
import { toast } from "react-toastify";

const DataRow = ({
  label,
  value,
  additionalClass = "",
  to,
  dynamicBg,
  type,
  type_value,
  payload,
}) => {
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const customId = "Custom-id-yes";
  const payloadObj = {
    ...payload,
    type: type,
    type_value: type_value,
  };

  const fetchData = async () => {
    if (to) {
      try {
        const actionResult = await dispatch(ReportData(payloadObj));
        console.log("actionResult", actionResult);
        if (actionResult?.payload?.success) {
          navigate(to);
        } else if (actionResult?.payload?.success === false) {
          toast.error(actionResult?.payload?.message, { toastId: customId });
        }
      } catch (error) {
        console.log("error while fetching data", error);
      }
    }
  };

  return (
    <div
      className={`col py-1 nested-card-6  ${additionalClass}`}
      onClick={fetchData}
      style={{
        backgroundColor: dynamicBg,
      }}
    >
      <p
        className="card-nested-content-para"
        style={{ color: dynamicBg === "yellow" ? "#212529" : "" }}
      >
        {label}
      </p>
      <p
        className="card-nested-content-para"
        style={{ color: dynamicBg === "yellow" ? "#212529" : "" }}
      >
        {value}
      </p>
    </div>
  );
};

export default DataRow;
