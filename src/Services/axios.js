import axios from "axios";
// import config from "../config";
// import { getLocalStorageToken } from "../utils";
// const { API_BASE_URL } = config;
import { API } from "./index";
const client = axios.create({});

client.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    return Promise.reject(error);
  }
);

const APIrequest = (endpoint, payload = {}, method = "get", headers = {}) => {
  const tokens = "11069|y6blU8pqh20hIyhfTOnyP5HBxaykhk24xy1ajXmE";

  if (tokens) {
    headers.token = tokens;
  }
  let axiosConfig = {
    method: method.toLowerCase(),
  };
  if (method === "get") {
    axiosConfig.params = payload;
  } else {
    axiosConfig.data = payload;
  }

  return client(`${API}${endpoint}`, axiosConfig);
};
export default APIrequest;
