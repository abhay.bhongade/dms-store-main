const API = "http://dmsapp.tekzee.in/api/distributor/";

const getLocalStorageToken = () => {
  const token = localStorage.getItem("token");
  return token;
};

console.log("getLocalStorageToken", getLocalStorageToken());

const getDistributorId = () => {
  const distributorId = localStorage.getItem("id");
  return distributorId;
};

console.log("getDistributorId", getDistributorId());

const config = {
  headers: {
    Authorization: `Bearer ${getLocalStorageToken()}`,
    // "Accept": "application/json",
    "Content-Type": "application/json",
    languagecode: "en",
  },
};

export { API, config, getDistributorId };
