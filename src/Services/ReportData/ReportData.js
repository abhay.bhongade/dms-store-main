import APIrequest from "../axios";

export const getReportData = (payload, token) => {
  console.log("payload", payload);
  console.log("token", token);
  return APIrequest("getDbInventoryStockStatusValue", payload, "POST", {
    token: token,
  });
};
