// import Store from "./Component/Store/Store";

import { Routes, Route } from "react-router-dom";
import {
  CustomerScoreCard,
  LazyCommanTable,
  LazyReportView,
  LazyStoreView,
  LazyStoreViewOnScroll,
  Store,
  LazyBeatsPendingTable,
  LazyBeatsOrdersTable,
  LazyBeatsDeliveredTable,
  LazyBeatsRejectedTable,
  LazyOrdersTotalTable,
  LazyOrdersAcceptedTable,
  LazyOrdersPendingTable,
  LazyOrdersRejectedTable,
  LazyDeliveryTotalBeatTable,
  LazyDeliveryTotalDistrictTable,
  LazyEscalationTotalCountTable,
  LazyPositiveEscalationCountTable,
  LazyNegativeEscalationCountTable,
} from "../Component";

import { NotFound } from "../Pages";

export default function RouteComponent() {
  const routesArr = [
    {
      path: "/",
      element: <Store />,
    },
    {
      path: "/store-view",
      element: <LazyStoreView />,
    },
    {
      path: "/store-view-scroll",
      element: <LazyStoreViewOnScroll />,
    },
    {
      path: "/report",
      element: <LazyReportView />,
    },
    {
      path: "/delivery",
      element: <LazyCommanTable />,
    },
    {
      path: "/next_page/customer-score-card",
      element: <CustomerScoreCard />,
    },

    {
      path: "*",
      element: <NotFound />,
    },
    {
      path: "/next_page/beats-pending-table",
      element: <LazyBeatsPendingTable />,
    },
    {
      path: "/next_page/beats-orders-table",
      element: <LazyBeatsOrdersTable />,
    },
    {
      path: "/next_page/beats-delivered-table",
      element: <LazyBeatsDeliveredTable />,
    },
    {
      path: "/next_page/beats-rejected-table",
      element: <LazyBeatsRejectedTable />,
    },

    {
      path: "/next_page/orders-pending-table",
      element: <LazyOrdersPendingTable />,
    },
    {
      path: "/next_page/orders-total-table",
      element: <LazyOrdersTotalTable />,
    },
    {
      path: "/next_page/orders-accepted-table",
      element: <LazyOrdersAcceptedTable />,
    },
    {
      path: "/next_page/orders-rejected-table",
      element: <LazyOrdersRejectedTable />,
    },

    {
      path: "/next_page/total-beats-table",
      element: <LazyDeliveryTotalBeatTable />,
    },
    {
      path: "/next_page/total-district-table",
      element: <LazyDeliveryTotalDistrictTable />,
    },

    {
      path: "/next_page/escalation-count-table",
      element: <LazyEscalationTotalCountTable />,
    },

    {
      path: "/next_page/positive-escalation-table",
      element: <LazyPositiveEscalationCountTable />,
    },
    {
      path: "/next_page/negative-escalation-table",
      element: <LazyNegativeEscalationCountTable />,
    },
  ];

  return (
    <Routes>
      {routesArr?.map((item, index) => (
        <Route path={item?.path} element={item?.element} key={index} />
      ))}
    </Routes>
  );
}
